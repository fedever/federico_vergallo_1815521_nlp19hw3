import tensorflow as tf
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Model
from tensorflow.keras.layers import LSTM, Embedding, Activation, concatenate, Concatenate, Input, Dense, Flatten, Lambda, TimeDistributed, multiply, Multiply, RepeatVector
from tensorflow.keras import initializers as initializers, regularizers, constraints
from tensorflow.keras.layers import InputSpec, Bidirectional
from tensorflow.keras.initializers import Constant
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras import backend as K
from tensorflow.keras.layers import Layer
from tensorflow.keras.utils import Sequence
import numpy as np
import pickle
from collections import Counter
import pandas as pd
from gensim.models import KeyedVectors, Word2Vec
import os
import statistics
import nltk

nltk.download('wordnet')
from nltk.corpus import wordnet as wn

from lxml import etree

# Some common wordnet transformations
def get_synset_from_sense(sense):
    return wn.synset_from_pos_and_offset(sense[-1], int(sense[-9:-1]))


def get_sense_from_synset(synset):
    return "wn:" + str(synset.offset()).zfill(8) + synset.pos()


def get_synset_from_sense_key(sense_key):
    return wn.lemma_from_key(sense_key).synset()


def get_sensekey_from_synset(synset):
    return synset.lemmas()[0].key()


def dump_to_file(output_path, pred_ids, y, wn2bn, task):
    # --- Save predictions to the file --- #
    sensekey_flag = False
    if output_path.split("/")[-1] == 'ALL.pred.txt':
        all_path = output_path.split(".pred.txt")[0]+".all.pred.txt"
        for sent_pred, sent_ids in zip(y, pred_ids):
            for word_sense, word_id in zip(sent_pred, sent_ids):
                if word_sense[-1] == 'n':
                    fn = 'noun'
                elif word_sense[-1] == 'v':
                    fn = 'verb'
                elif word_sense[-1] == 'a' or word_sense[-1] == 's':
                    fn = 'adj'
                elif word_sense[-1] == 'r':
                    fn = 'adv'
                else:
                    raise Exception
                path = output_path.split(".pred.txt")[0]+"."+fn+".pred.txt"
                if task == 'WSD':
                    if sensekey_flag:
                        pred_word = get_sensekey_from_synset(get_synset_from_sense(word_sense))
                    else:
                        pred_word = wn2bn[word_sense]
                else:
                    pred_word = word_sense
                with open(path, "a") as f:
                    f.write(word_id + " " + pred_word + " \n")
                with open(all_path, "a") as f:
                    f.write(word_id + " " + pred_word + " \n")
    else:
        with open(output_path, "w") as f:
            for sent_pred, sent_ids in zip(y, pred_ids):
                for word_sense, word_id in zip(sent_pred, sent_ids):
                    if task == 'WSD':
                        if sensekey_flag:
                            pred_word = get_sensekey_from_synset(get_synset_from_sense(word_sense))
                        else:
                            pred_word = wn2bn[word_sense]
                    else:
                        pred_word = word_sense

                    f.write(word_id + " " + pred_word + " \n")


def load_common_files(resource_path):
    '''
    Load common files for the task

    :param  resource_path : string common resource path
    :return wn2bn         : dict mapping from wn to bn
    :return bn2wn		  : dict mapping from bn to wn
    :return bn2lex		  : dict mapping from bn to wn lex
    :return bn2dom	      : dict mapping from bn to wn domains
    '''
    bn2wn_file = resource_path + "babelnet2wordnet.tsv"
    wn2bn = {x.split('\t')[1].strip("\n"): x.split('\t')[0] for x in open(bn2wn_file).readlines()}
    bn2wn = {x.split('\t')[0]: x.split('\t')[1].strip("\n") for x in open(bn2wn_file).readlines()}

    babelnet2lexnames_file = resource_path + "babelnet2lexnames.tsv"
    bn2lex = {x.split('\t')[0].strip("\n"): x.split('\t')[1].strip("\n") for x in open(babelnet2lexnames_file).readlines()}

    babelnet2wndomains_file = resource_path + "babelnet2wndomains.tsv"
    babelnet2wndomains_pre = {x.split('\t')[0].strip("\n"): x.split('\t')[1:] for x in open(babelnet2wndomains_file).readlines()}
    bn2dom = {}

    for bn in babelnet2wndomains_pre:
        bn2dom[bn] = "_".join(babelnet2wndomains_pre[bn]).strip("\n")

    return wn2bn, bn2wn, bn2lex, bn2dom


def load_output_dict(resource_path, compression_flag):
    '''
    Load and build the output dictionary of the model

    :param  resource_path       : string common resource path
    :param  compression_flag    : bool True if want to use the compressed vocabulary
    :return original2compressed : dict if compression_flag is true, it returns the mapping from original sense to compressed one. None otherwise
    :return compressed2original : dict if compression_flag is true, it returns the mapping from compressed sense to original ones. None otherwise
    :return label_to_id         : dict mapping from output word to index
    :return id_to_label		    : dict mapping from index to output word
    '''
    if compression_flag:
        original2compressed_sense_df = pd.read_csv(resource_path + "original2compressed_semcor_sense_mapping.csv", delimiter="\t")
        original2compressed = {}
        for index, row in original2compressed_sense_df.iterrows():
            original2compressed[row['original']] = row['compressed']

        compressed2original = {}
        for k, v in original2compressed.items():
            if v not in compressed2original:
                compressed2original[v] = []
            compressed2original[v].append(k)

        with open(resource_path + "label_to_id.pkl", "rb") as f:
            label_to_id = pickle.load(f)
        with open(resource_path + "id_to_label.pkl", "rb") as f:
            id_to_label = pickle.load(f)
        return original2compressed, compressed2original, label_to_id, id_to_label

    else:
        with open(resource_path + "label_to_id_nocompression.pkl", "rb") as f:
            label_to_id = pickle.load(f)
        with open(resource_path + "id_to_label_nocompression.pkl", "rb") as f:
            id_to_label = pickle.load(f)
        return None, None, label_to_id, id_to_label


def load_embeddings(resource_path):
    '''
    Load and build the embeddings matrix and the input vocabulary

    :param  resource_path       : string common resource path
    :return embedding_matrix	: dict embedding matrix for embedding layer
    :return token_to_id         : dict mapping from input word to index
    :return id_to_token	        : dict mapping from index to input word
    '''
    with open(resource_path+"embedding_matrix_glove.pkl", "rb") as f:
        embedding_matrix = pickle.load(f)

    # ---- Load token2id vocab ----
    with open(resource_path+"token_to_id_glove.pkl", "rb") as f:
        token_to_id = pickle.load(f)
    # ---- Load id2token vocab ----
    with open(resource_path+"id_to_token_glove.pkl", "rb") as f:
        id_to_token = pickle.load(f)

    return embedding_matrix, token_to_id, id_to_token


def load_domain_vocab(resource_path):
    '''
    Load and build the domains vocabulary

    :param  resource_path       : string common resource path
    :return domain_to_id        : dict mapping from output domain to index
    :return id_to_domain        : dict mapping from index to output domain
    '''
    with open(resource_path + "domain_to_id.pkl", "rb") as f:
        domain_to_id = pickle.load(f)
    with open(resource_path + "id_to_domain.pkl", "rb") as f:
        id_to_domain = pickle.load(f)

    return domain_to_id, id_to_domain


def load_lex_vocab(resource_path):
    '''
    Load and build the domains vocabulary

    :param  resource_path   : string common resource path
    :return lex_to_id       : dict mapping from output lex to index
    :return id_to_lex       : dict mapping from index to output lex
    '''
    with open(resource_path + "lex_to_id.pkl", "rb") as f:
        lex_to_id = pickle.load(f)
    with open(resource_path + "id_to_lex.pkl", "rb") as f:
        id_to_lex = pickle.load(f)

    return lex_to_id, id_to_lex


class AttentionWeightedAverage(Layer):
    '''
    Following Raganato's et. al paper, It computes the attention vector
    as the weighted sum of the hidden state vectors.
    :param inputs:
    :param lstm_units:
    :param MAX_LENGHT:
    :return:
    '''

    def __init__(self, return_attention=False, **kwargs):
        self.init = initializers.get('uniform')
        self.supports_masking = True
        self.return_attention = return_attention
        super(AttentionWeightedAverage, self).__init__(**kwargs)

    def build(self, input_shape):
        self.input_spec = [InputSpec(ndim=3)]
        assert len(input_shape) == 3

        self.w = self.add_weight(shape=(input_shape[2].value, 1),
                                 name='{}_w'.format(self.name),
                                 initializer=self.init,
                                 trainable=True)
        # self.trainable_weights = self.w
        super(AttentionWeightedAverage, self).build(input_shape)

    def call(self, h, mask=None):
        h_shape = K.shape(h)
        d_w, T = h_shape[0], h_shape[1]

        logits = K.dot(h, self.w)  # w^T h
        logits = K.reshape(logits, (d_w, T))
        alpha = K.exp(logits - K.max(logits, axis=-1, keepdims=True))  # exp

        # masked timesteps have zero weight
        if mask is not None:
            mask = K.cast(mask, K.floatx())
            alpha = alpha * mask
        alpha = alpha / K.sum(alpha, axis=1, keepdims=True)  # softmax
        c = K.sum(h * K.expand_dims(alpha), axis=1)  # c = h*alpha^T
        c_star = K.tanh(c)  # c^* = tanh(c)
        if self.return_attention:
            return [c_star, alpha]
        return c_star

    def get_output_shape_for(self, input_shape):
        return self.compute_output_shape(input_shape)

    def compute_output_shape(self, input_shape):
        output_len = input_shape[2]
        if self.return_attention:
            return [(input_shape[0], output_len), (input_shape[0], input_shape[1])]
        return (input_shape[0], output_len)

    def compute_mask(self, input, input_mask=None):
        if isinstance(input_mask, list):
            return [None] * len(input_mask)
        else:
            return None


### ---- Fun part: Model! ----
def build_wsd_model(vectors_number, EMBEDDING_DIM, embedding_matrix, lstm_units, MAX_LENGHT, num_senses, num_domains, num_lex, attention_flag, flag_multitask):
    '''
    Build the model

    :param vectors_number  : int number of vectors in the embedding layer
    :param EMBEDDING_DIM   : int number of vectors dimension in the embedding layer
    :param embedding_matrix: dict of vectors for the embedding layer
    :param MAX_LENGHT	   : int number of sentence length
    :param attention_flag  : bool True if want to add attention mechanism, False otherwise
    :param flag_multitask  : int True if want to use multitask model, False otherwise
    :return attention: tensor
    '''

    # Imposing embedding layer to use pretrained embeddings
    embedding_layer = Embedding(vectors_number,
                                EMBEDDING_DIM,
                                weights=[embedding_matrix],
                                mask_zero=True,
                                trainable=False)

    sequence_input = Input(shape=(MAX_LENGHT,))
    embedded_sequences = embedding_layer(sequence_input)

    if attention_flag:
        bilstm = Bidirectional(LSTM(units=lstm_units, return_sequences=True, recurrent_dropout=0.3))(embedded_sequences)
        c, alpha = AttentionWeightedAverage(return_attention=True)(bilstm)
        repeated_c = RepeatVector(MAX_LENGHT)(c)
        repeated_out = Lambda(lambda x: Concatenate()([x, repeated_c]))(bilstm)
        preds_wsd = TimeDistributed(Dense(num_senses, activation='softmax'), name='wsd')(repeated_out)
        if flag_multitask:
            preds_domains = TimeDistributed(Dense(num_domains, activation='softmax'), name='domains')(repeated_out)
            preds_lex = TimeDistributed(Dense(num_lex, activation='softmax'), name='lex')(repeated_out)
    else:
        bilstm = Bidirectional(LSTM(units=lstm_units, return_sequences=True, recurrent_dropout=0.3))(embedded_sequences)
        preds_wsd = TimeDistributed(Dense(num_senses, activation='softmax'), name='wsd')(bilstm)
        if flag_multitask:
            preds_domains = TimeDistributed(Dense(num_domains, activation='softmax'), name='domains')(bilstm)
            preds_lex = TimeDistributed(Dense(num_lex, activation='softmax'), name='lex')(bilstm)

    if flag_multitask:
        wsd_model = Model(inputs=sequence_input,
                          outputs=[preds_wsd, preds_domains, preds_lex])
    else:
        wsd_model = Model(inputs=sequence_input,
                          outputs=[preds_wsd])

    return wsd_model


def process_input(text, target_dict):
    '''
    Transforms a sentence in an array of numbers for feeding the model

    :param text 	          : string sentence to transform
    :param target_dict	      : dict   mapping to use for transformation
    :return res               : array  word index from input vocab
    '''

    text = text.lower().split()
    res = []
    for word in text:
        if word in target_dict:
            res.append(target_dict[word])
        else:
            res.append(target_dict["<UNK>"])
    return np.array([res])


def array_slicing(arr, MAX_LENGTH, text_len):
    '''
    Splits an array in chunk of fixed length

    :param 	arr   : array  array to split
    :param	MAX_LENGTH  : int	   length of array chunks
    :return output: array  array of array chunks
    '''

    output = []
    if arr.shape[1] <= MAX_LENGTH:
        return arr
    for i in range(0, arr.shape[1] // MAX_LENGTH + 1):
        if MAX_LENGTH * i + MAX_LENGTH <= arr.shape[1]:
            new_arr = arr[0, MAX_LENGTH * i:MAX_LENGTH * i + MAX_LENGTH]
        else:
            new_arr = arr[0, MAX_LENGTH * i:]
        if new_arr.size > 0:
            output.append([new_arr])

    slice_number = 0
    while text_len >= 0:
        slice_number += 1
        text_len -= MAX_LENGTH

    while len(output) < slice_number:
        output.append([])

    return np.asarray(output)


def text_split(text_arr, num):
    '''
    Splits a text list in chunk of fixed length

    :param 	text_arr   : array array to split
    :param	num  	   : int   length of array chunks
    :return output	   : list  list of list chunks
    '''

    return [text_arr[x:x + num] for x in range(0, len(text_arr), num)]


def split_index2disambiguate(MAX_LENGTH, index2disambiguate, text_len):
    '''
    Since we split the sentence in chunk, we have to do so also for the indeces to disambiguate.
    We need to modify the index according to the position that the word to be disambiguated will
    be in the chunk. We divide index2disambiguate in lists by putting together indeces in range
    [k*MAX_LENGTH , k*MAX_LENGTH + MAX_LENGTH]

    :param MAX_LENGTH           : int  sentence max length
    :param index2disambiguate   : list list of index of word sense
    :param text_len             : int  length of the sentence
    :return: res                : list of list of index2disambiguate for each chunk
    '''
    threshold = MAX_LENGTH
    res = []
    res_slice = []
    to_subtract = 0
    for el in index2disambiguate:
        if el == index2disambiguate[-1]:
            if el >= threshold:
                res.append(res_slice)
                to_subtract += threshold
                res.append([el - to_subtract])
            else:
                res_slice.append(el - to_subtract)
                res.append(res_slice)
            break
        if el < threshold:
            res_slice.append(el - to_subtract)
        else:
            while el >= threshold:
                to_subtract += threshold
                threshold += threshold
            res.append(res_slice)
            res_slice = [el - to_subtract]

    slice_number = 0
    while text_len >= 0:
        slice_number += 1
        text_len -= MAX_LENGTH

    while len(res) < slice_number:
        res.append([])
    return res


def get_senses_of_words(text, index2disambiguate, compression_flag, original2compressed):
    '''
    Compute the output C candidates list of word senses for each word that has to be disambiguated

    :param text 			  : list  sentence
    :param index2disambiguate : list  list of index position of words that has to be disambiguated
    :param compression_flag   : bool  True if compression strategy
    :param original2compressed: dict  map for compression. None if compression_flag is False
    :return res				  : list  list of both word and word senses list rispectively for token word and word to disambiguate
    '''

    res = []
    for i in range(len(text)):
        if i in index2disambiguate:
            synsets = wn.synsets(text[i])
            senses = [get_sense_from_synset(synset) for synset in synsets]
            C = []
            for sense in senses:
                if compression_flag:
                    if sense in original2compressed:
                        C.append(original2compressed[sense])
                    else:
                        C.append(sense)
                else:
                    C.append(sense)
            res.append(C)
        else:
            res.append(text[i])
    return res


def get_index_from_vocab(sense, target_dict, compression_flag, original2compressed):
    '''
    Compute the index of a sense in the output vocabulary

    :param sense 			  : string  word sense
    :param target_dict		  : dict   output vocabulary
    :param compression_flag   : bool   True if compression strategy
    :param original2compressed: dict   map for compression. None if compression_flag is False
    :return output			  : int    the index if word sense is in the output vocabulary, None otherwise
    '''

    if compression_flag:
        if sense in original2compressed:
            sense = original2compressed[sense]
    if sense in target_dict:
        return target_dict[sense]
    else:
        return None


def get_index_of_sense_in_string(lst):
    '''
    Compute the index of word senses in a string
    :param lst : list input word list
    :return res: list of word senses index
    '''

    res = []
    for i in range(len(lst)):
        if 'wn:' in lst[i]:
            res.append(i)
    return res


def get_original_sense_from_compressed2(preds, x_text_splitted, compressed2original):
    result = []
    for word, token in zip(preds, x_text_splitted):
        if 'wn:' in word:
            if word in compressed2original:
                if len(compressed2original[word]) > 1:
                    token_synsets = wn.synsets(token)
                    original_senses = [get_synset_from_sense(sense) for sense in compressed2original[word]]
                    result_synset = list(set(token_synsets) & set(original_senses))
                    try:
                        result.append(get_sense_from_synset(result_synset[0]))
                    except:
                        try:
                            syn = [get_synset_from_sense(word)]
                            result_synset = list(set(token_synsets) & set(syn))
                            result.append(get_sense_from_synset(result_synset[0]))
                        except:
                            print(token)
                            print(token_synsets)
                            print(word)
                            print(original_senses)
                else:
                    result.append(word)
            else:
                result.append(word)
        else:
            result.append(word)
    return result


def get_original_sense_from_compressed(preds, x_text_splitted, compressed2original):
    '''
    Returns the predictions list of strings substituing the compressed sense output with the original sense.
    For doing so, takes the intersection between the possible original word synsets list
    and the list of original synset which the compressed sense could refer to.

    :param preds		      : list of strings of predictions result text
    :param x_text_splitted    : list of strings of input text
    :param compressed2original: map between compressed sense and original ones
    :return result			  : list of resulting tokens with decompressed word sense
    '''

    result = []
    for word, token in zip(preds, x_text_splitted):
        if 'wn:' in word:
            if word in compressed2original:
                if len(compressed2original[word]) > 1:
                    #﻿ Find all the synsets related to the original word to map
                    token_synsets = wn.synsets(token)
                    # Find
                    original_senses = [get_synset_from_sense(sense) for sense in compressed2original[word]]
                    result_synset = list(set(token_synsets) & set(original_senses))
                    try:
                        result.append(get_sense_from_synset(result_synset[0]))
                    except:
                        try:
                            syn = [get_synset_from_sense(word)]
                            result_synset = list(set(token_synsets) & set(syn))
                            result.append(get_sense_from_synset(result_synset[0]))
                        except:
                            print(token)
                            print(token_synsets)
                            print(word)
                            print(original_senses)
                else:
                    result.append(word)
            else:
                result.append(word)
        else:
            result.append(word)
    return result


def get_property_from_senses(text_senses, wn2bn, bn2prop, task):
    text_senses_result = []
    for el in text_senses:
        if type(el) == list:
            prop_out = []
            for word_sense in el:
                if word_sense in wn2bn:
                    if wn2bn[word_sense] in bn2prop:
                        prop_out.append(bn2prop[wn2bn[word_sense]])
                    else:
                        if task == 'DOM':
                            prop_out.append('factotum')
                        else:
                            prop_out.append('none')
                else:
                    if task == 'DOM':
                        prop_out.append('factotum')
                    else:
                        prop_out.append('none')
            text_senses_result.append(list(set(prop_out)))
        elif 'wn:' in el:
            if el in wn2bn:
                if wn2bn[el] in bn2prop:
                    text_senses_result.append(bn2prop[wn2bn[el]])
                else:
                    if task == 'DOM':
                        text_senses_result.append('factotum')
                    else:
                        text_senses_result.append('none')
            else:
                if task == 'DOM':
                    text_senses_result.append('factotum')
                else:
                    text_senses_result.append('none')

        else:
            text_senses_result.append(el)

    return text_senses_result


def get_prediction_result(text, preds, index2disambiguate, target_to_id, id_to_target, compression_flag, original2compressed, task, wn2bn, bn2prop):
    '''
    Process the prediction result. Strategy:
    If token word: return the word
    If word sense:
        - Collect all the senses of original word
        For each word sense:
            - Build C list using the index of word senses in the output vocabulary
            - If C is empty:
                use MFS strategy
            - If C len is 1:
                return the only one available word sense
            - If C len is more than 1:
                get the argmax value of predictions among the values defined in C

    :param	text 				: list   input words
    :param	preds	    		: tensor predictions from model
    :param  index2disambiguate	: list   indeces of words to disambiguate
    :param  target_to_id         : dict   id to content mapping
    :param  id_to_target         : dict   content to id mapping
    :param  compression_flag    : bool   True if compression strategy
    :param  original2compressed : dict   Compressed sense vocabulary
    :param  task                 : string What task to perform
    :param  wn2bn               : dict mapping wn to bn sense
    :param  bn2prop             : dict mapping bn to a property (lex or dom)
    :return pred_result         : list of both token word and word senses
    '''
    preds = np.squeeze(preds)
    text_senses = get_senses_of_words(text, index2disambiguate, compression_flag, original2compressed)
    mfs_count = 0
    if task != 'WSD':
        text_senses = get_property_from_senses(text_senses, wn2bn, bn2prop, task)
    pred_result = []
    for i in range(len(text_senses)):
        if type(text_senses[i]) == list:
            # Get the index of senses in the vocab
            C = [get_index_from_vocab(sense, target_to_id, compression_flag, original2compressed) for sense in text_senses[i]]
            C = list(set([el for el in C if el is not None]))
            if len(C) == 0:
                # MFS strategy: in WordNet senses are sorted by frequency, so I pick the first
                try:
                    if task == 'WSD':
                        mfs_count += 1
                        pred_result.append(get_sense_from_synset(wn.synsets(text[i])[0]))
                    elif task == 'DOM' or task == 'LEX':
                        # I take the domain of the MFS
                        mfs_sense = get_sense_from_synset(wn.synsets(text[i])[0])
                        if mfs_sense in wn2bn:
                            if wn2bn[mfs_sense] in bn2prop:
                                pred_result.append(bn2prop[wn2bn[mfs_sense]])
                            else:
                                if task == 'DOM':
                                    pred_result.append('factotum')
                                else:
                                    pred_result.append('none')
                        else:
                            if task == 'DOM':
                                pred_result.append('factotum')
                            else:
                                pred_result.append('none')
                except:
                    print(text)
                    print(text[i])
                    print(wn.synsets(text[i]))
            elif len(C) == 1:
                pred_result.append(id_to_target[C[0]])
            else:
                prob_values = np.array([preds[i, c] for c in C])
                output_sense = C[np.argmax(prob_values)]
                pred_result.append(id_to_target[output_sense])
        else:
            pred_result.append(text_senses[i])
    return pred_result, mfs_count


def predict_sentence(X, token_to_id, wsd_model, index2disambiguate, MAX_LENGTH, target_to_id, id_to_target, compression_flag,
                     compressed2original, original2compressed, flag_multitask, task, wn2bn, bn2prop):
    '''
    Main function to predict a sentence. Five main steps:
    1 - Preprocess the input sentence
    2 - Split the input sentence in chunk of MAX_LENGTH length
    3 - For each sentence chunk, predict the word senses
    4 - Transform the predictions in word senses taking the argmax of each word predictions
    5 - If we compressed the output vocab, find the uncompressed word senses

    :param X				  : string the input sentence
    :param wsd_model		  : model  trained model
    :param token_to_id 		  : dict   input dictionary
    :param index2disambiguate : list   of word indeces to disambiguate
    :param MAX_LENGTH         : int    padding size
    :param target_to_id 	  : dict   content to id output dictionary
    :param id_to_target 	  : dict   id to content output dictionary
    :param compression_flag   : bool   True if compression strategy
    :param compressed2original: dict   map between compressed sense and original ones
    :param original2compressed: dict   map between original senses and  compressed one
    :param flag_multitask  	  : bool   True if want to use multitask model, False otherwise
    :param task				  : string name of the task to predict
    :param  wn2bn             : dict mapping wn to bn sense
    :param  bn2prop           : dict mapping bn to a property (lex or dom)
    :return result			  : list   of word senses/domains/lex
    '''

    x_input = process_input(X, token_to_id)
    result_line = []
    text_len = len(X.split())
    mfs_total_count = 0
    for x_slice, x_text_splitted, indeces in zip(array_slicing(x_input, MAX_LENGTH, text_len),
                                                 text_split(X.split(), MAX_LENGTH),
                                                 split_index2disambiguate(MAX_LENGTH, index2disambiguate, text_len)):
        # Pad if necessary
        try:
            x_slice = pad_sequences(x_slice, truncating='pre', padding='post', maxlen=MAX_LENGTH, value=0)
        except ValueError:
            x_slice = pad_sequences([x_slice], truncating='pre', padding='post', maxlen=MAX_LENGTH, value=0)

        # Predict
        preds = wsd_model.predict(x_slice)

        # Process predictions
        if flag_multitask:
            if task == 'WSD':
                preds_to_use = preds[0]
            elif task == 'DOM':
                preds_to_use = preds[1]
            else: #LEX
                preds_to_use = preds[2]
        else:
            preds_to_use = preds[0]
        preds_processed, mfs_count = get_prediction_result(x_text_splitted, preds_to_use, indeces, target_to_id, id_to_target,
                                                compression_flag, original2compressed, task, wn2bn, bn2prop)
        mfs_total_count += mfs_count
        # Get the original sense from compressed predictions and append
        if compression_flag:
            result_line.append(get_original_sense_from_compressed(preds_processed, x_text_splitted, compressed2original))
        else:
            result_line.append(preds_processed)

    # Return result
    result = sum(result_line, [])

    return result, mfs_total_count


def predict_babelnet(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <BABELSynset>" format (e.g. "d000.s000.t000 bn:01234567n").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    compression_flag = True
    flag_multitask = False
    attention_flag = False

    # Load useful variables
    wn2bn, bn2wn, bn2lex, bn2dom = load_common_files(resources_path)
    # Load output vocabulary
    original2compressed, compressed2original, label_to_id, id_to_label = load_output_dict(resources_path, compression_flag)
    # Load embeddings and input vocabulary
    embedding_matrix, token_to_id, id_to_token = load_embeddings(resources_path)

    if flag_multitask:
        domain_to_id, id_to_domain = load_domain_vocab(resources_path)
        lex_to_id, id_to_lex = load_lex_vocab(resources_path)
        num_domains = len(id_to_domain.keys())
        num_lex = len(id_to_lex.keys())
    else:
        num_domains, num_lex = 0, 0

    # Params of the model
    MAX_LENGHT = 57
    lstm_units = 256
    EMBEDDING_DIM = embedding_matrix.shape[1]
    vectors_number = embedding_matrix.shape[0]
    num_senses = len(label_to_id)

    wsd_model = build_wsd_model(vectors_number, EMBEDDING_DIM, embedding_matrix, lstm_units, MAX_LENGHT, num_senses, num_domains, num_lex, attention_flag, flag_multitask)

    wsd_model.load_weights(resources_path + "compression_blstm.h5")

    # Load the xml file
    context = etree.iterparse(input_path, tag='sentence')
    task = 'WSD'
    pred_ids = []  # list of list of instance ids
    y = []  # list of list of word sense
    mfs_total_count = 0
    for event, sentence in context:
        text = []
        index2disambiguate = []
        ids_sent = []  # It will contain the id for each sentences
        i = 0

        for tag in sentence:
            # Skipping punctuation
            if tag.attrib['pos'] == ".":
                continue

            if tag.tag == 'instance':
                ids_sent.append(tag.attrib['id'])
                index2disambiguate.append(i)

            text.append(tag.attrib['lemma'].lower())
            i += 1

        sentence.clear()

        # Predict sentence
        result, mfs_count = predict_sentence(' '.join(text), token_to_id, wsd_model, index2disambiguate, MAX_LENGHT, label_to_id, id_to_label, compression_flag,
                                  compressed2original, original2compressed, flag_multitask, task, wn2bn, None)
        mfs_total_count += mfs_count
        # Get word senses that has been disambiguate
        y_pred = [result[i] for i in index2disambiguate]
        try:
            assert len(y_pred) == len(index2disambiguate)
        except AssertionError:
            print("Assertion Error")
            print(text)
            print(index2disambiguate)
            print(result)
            print(y_pred)

        # Append to collective variable
        pred_ids.append(ids_sent)
        y.append(y_pred)

    # Dump to file
    dump_to_file(output_path, pred_ids, y, wn2bn, task)

    pass


def predict_wordnet_domains(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <wordnetDomain>" format (e.g. "d000.s000.t000 sport").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    compression_flag = False
    flag_multitask = True
    attention_flag = True

    # Load useful variables
    wn2bn, bn2wn, bn2lex, bn2dom = load_common_files(resources_path)
    # Load output vocabulary
    original2compressed, compressed2original, label_to_id, id_to_label = load_output_dict(resources_path, compression_flag)
    # Load embeddings and input vocabulary
    embedding_matrix, token_to_id, id_to_token = load_embeddings(resources_path)

    # Load domains and lex vocab
    domain_to_id, id_to_domain = load_domain_vocab(resources_path)
    lex_to_id, id_to_lex = load_lex_vocab(resources_path)

    # Params of the model
    MAX_LENGHT = 57
    lstm_units = 256
    EMBEDDING_DIM = embedding_matrix.shape[1]
    vectors_number = embedding_matrix.shape[0]
    num_senses  = len(label_to_id)
    num_domains = len(id_to_domain)
    num_lex     = len(id_to_lex)

    # Build the model and load the weights
    wsd_model = build_wsd_model(vectors_number, EMBEDDING_DIM, embedding_matrix, lstm_units, MAX_LENGHT, num_senses, num_domains, num_lex, attention_flag, flag_multitask)
    wsd_model.load_weights(resources_path + "nocompression_blstm_att_dom_lex.h5")

    # Load the xml file
    context = etree.iterparse(input_path, tag='sentence')
    task = 'DOM'
    pred_ids = []  # list of list of instance ids
    y = []  # list of list of word domains

    for event, sentence in context:
        text = []
        index2disambiguate = []
        ids_sent = []  # It will contain the id for each sentences
        i = 0

        for tag in sentence:
            # Skipping punctuation
            if tag.attrib['pos'] == ".":
                continue

            if tag.tag == 'instance':
                ids_sent.append(tag.attrib['id'])
                index2disambiguate.append(i)
            text.append(tag.attrib['lemma'].lower())
            i += 1


        sentence.clear()

        # Predict sentence
        result, mfs_count = predict_sentence(' '.join(text), token_to_id, wsd_model, index2disambiguate, MAX_LENGHT, domain_to_id, id_to_domain,
                                  compression_flag, compressed2original, original2compressed, flag_multitask, task, wn2bn, bn2dom)

        # Get word senses that has been disambiguate
        try:
            y_pred = [result[i] for i in index2disambiguate]
        except:
            print(index2disambiguate)
            print(result)
            print(ids_sent)
        try:
            assert len(y_pred) == len(index2disambiguate)
        except AssertionError:
            print("Assertion Error")
            print(text)
            print(index2disambiguate)
            print(result)
            print(y_pred)

        # Append to collective variable
        pred_ids.append(ids_sent)
        y.append(y_pred)


    # Dump to file
    dump_to_file(output_path, pred_ids, y, wn2bn, task)
    
    pass


def predict_lexicographer(input_path: str, output_path: str, resources_path: str) -> None:
    """
    DO NOT MODIFY THE SIGNATURE!
    This is the skeleton of the prediction function.
    The predict function will build your model, load the weights from the checkpoint and write a new file (output_path)
    with your predictions in the "<id> <lexicographerId>" format (e.g. "d000.s000.t000 noun.animal").

    The resources folder should contain everything you need to make the predictions. It is the "resources" folder in your submission.

    N.B. DO NOT HARD CODE PATHS IN HERE. Use resource_path instead, otherwise we will not be able to run the code.
    If you don't know what HARD CODING means see: https://en.wikipedia.org/wiki/Hard_coding

    :param input_path: the path of the input file to predict in the same format as Raganato's framework (XML files you downloaded).
    :param output_path: the path of the output file (where you save your predictions)
    :param resources_path: the path of the resources folder containing your model and stuff you might need.
    :return: None
    """
    compression_flag = False
    flag_multitask = True
    attention_flag = True

    # Load useful variables
    wn2bn, bn2wn, bn2lex, bn2dom = load_common_files(resources_path)
    # Load output vocabulary
    original2compressed, compressed2original, label_to_id, id_to_label = load_output_dict(resources_path, compression_flag)
    # Load embeddings and input vocabulary
    embedding_matrix, token_to_id, id_to_token = load_embeddings(resources_path)

    # Load domains and lex vocab
    domain_to_id, id_to_domain = load_domain_vocab(resources_path)
    lex_to_id, id_to_lex = load_lex_vocab(resources_path)

    # Params of the model
    MAX_LENGHT = 57
    lstm_units = 256
    EMBEDDING_DIM = embedding_matrix.shape[1]
    vectors_number = embedding_matrix.shape[0]
    num_senses = len(label_to_id)
    num_domains = len(id_to_domain)
    num_lex = len(id_to_lex)

    # Build the model and load the weights
    wsd_model = build_wsd_model(vectors_number, EMBEDDING_DIM, embedding_matrix, lstm_units, MAX_LENGHT, num_senses, num_domains, num_lex, attention_flag, flag_multitask)

    wsd_model.load_weights(resources_path + "nocompression_blstm_att_dom_lex.h5")

    # Load the xml file
    context = etree.iterparse(input_path, tag='sentence')
    task = 'LEX'
    pred_ids = []  # list of list of instance ids
    y = []  # list of list of word domains

    for event, sentence in context:
        text = []
        index2disambiguate = []
        ids_sent = []  # It will contain the id for each sentences
        i = 0

        for tag in sentence:
            # Skipping punctuation
            if tag.attrib['pos'] == ".":
                continue

            if tag.tag == 'instance':
                ids_sent.append(tag.attrib['id'])
                index2disambiguate.append(i)
            text.append(tag.attrib['lemma'].lower())
            i += 1

        sentence.clear()

        # Predict sentence
        result, mfs_count = predict_sentence(' '.join(text), token_to_id, wsd_model, index2disambiguate, MAX_LENGHT, lex_to_id, id_to_lex,
                                  compression_flag, compressed2original, original2compressed, flag_multitask, task, wn2bn, bn2lex)

        # Get word senses that has been disambiguate
        try:
            y_pred = [result[i] for i in index2disambiguate]
        except:
            print(index2disambiguate)
            print(result)
            print(ids_sent)
        try:
            assert len(y_pred) == len(index2disambiguate)
        except AssertionError:
            print("Assertion Error")
            print(text)
            print(index2disambiguate)
            print(result)
            print(y_pred)

        # Append to collective variable
        pred_ids.append(ids_sent)
        y.append(y_pred)

    # Dump to file
    dump_to_file(output_path, pred_ids, y, wn2bn, task)
    pass
