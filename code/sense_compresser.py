# -*- coding: utf-8 -*-
import nltk
from nltk.corpus import wordnet as wn
import pandas as pd
import tqdm
nltk.download('wordnet')


def get_synset_from_sense(sense):
    return wn.synset_from_pos_and_offset(sense[-1], int(sense[-9:-1]))


def get_sense_from_synset(synset):
    return "wn:" + str(synset.offset()).zfill( 8) + synset.pos()


def tessa(source):
    '''
    Create the pairs in a list
    :param source: list
    :return: result: list of pairs
    '''
    result = []
    for p1 in range(len(source)):
            for p2 in range(p1+1,len(source)):
                    result.append([source[p1],source[p2]])
    return result


def get_words_senses_lemma(y):
    '''
    Collects all the words that have to disambiguate and save them in a
    list of word sense and a list of lemmas.
    :param y: list of sentences
    :return: input_word_senses: list of word sense
    :return: y_words: list of words to disambiguate
    '''
    y_words = []
    input_word_senses = []
    for sentence in tqdm.tqdm(y, total=len(y)):
        for word in sentence.strip("\n").split(" "):
            if "wn:" in word:
                input_word_senses.append(word)
                sense_name = get_synset_from_sense(word).lemma_names()[0]
                if sense_name not in y_words:
                    y_words.append(sense_name)
    return input_word_senses, y_words


def get_necessary_synsets(y_words):
    '''
    Collect the synsets that have to be saved which are the one whose word sense
    cannot be mapped with one ancestor. For doing so, for each word that has
    to be disambiguated:
        1 - Take all the synsets of the word
        2 - Make pairs of two synsets of that word
        3 - For each pair:
            1 - Get the common lowest hypernym
            2 - Mark the children of this as necessary
    :param y_words: list of words that have to be disambiguate
    :return: necessary_synsets: list of synstes that cannot be mapped
    '''
    necessary_synsets = []
    for word in tqdm.tqdm(y_words, total=len(y_words)):
        word_synsets = wn.synsets(word)
        senses_pairs = tessa(word_synsets)
        for pair in senses_pairs:
            # Get the common lowest hypernym
            try:
                lwst_hypernym = pair[0].lowest_common_hypernyms(pair[1])[0]
                children = lwst_hypernym.hyponyms()
                for child in children:
                    if child not in necessary_synsets:
                        necessary_synsets.append(child)
            except IndexError:
                print("Error in pairs: ", pair[0].lemma_names()[0], " ", pair[1].lemma_names()[0])
                print("synsets : ", pair[0], " ",  pair[1])
                print("with senses : ", get_sense_from_synset(pair[0]), " ",  get_sense_from_synset(pair[1]))
                print("Definitions : ", pair[0].definition(), "\t", pair[1].definition())
                print("Lowest common hypernyms: ", pair[0].lowest_common_hypernyms(pair[1]))
                print("---"*5)
                continue
        return necessary_synsets


def map_to_ancestor(input_word_senses, necessary_synsets):
    ''' Map a synset with its lowest necessary ancestor (hypernym).
    :param input_word_senses: list of input word senses from corpus
    :param necessary_synsets: list of necessary synsets
    :return: synset2necessary_hypernym: dict map descendent synset to lowest possible synset ancestor
    '''
    synset2necessary_hypernym = {}
    synsets_list = [get_synset_from_sense(word_sense) for word_sense in list(set(input_word_senses))]
    for synset in tqdm.tqdm(synsets_list, total=len(synsets_list)):
        # Get the list of hypernyms
        hypernyms_path = synset.hypernym_paths()[0]
        # Iterate to reversed hypernyms path
        for i in range(len(hypernyms_path)-1, -1, -1):
            # Stop whenever find a necessary hypernym:
            if hypernyms_path[i] in necessary_synsets:
                # Map and stop
                if synset not in synset2necessary_hypernym:
                    synset2necessary_hypernym[synset] = hypernyms_path[i]
                    break
    return synset2necessary_hypernym


if __name__ == '__main__':
    path = '../resources/'

    # Open labels files
    with open("../resources/y_semcor_label.txt", "r") as f:
        y = f.readlines()

    # Collect word sense and lemma that have to be disambiguated
    input_word_senses, y_words = get_words_senses_lemma(y)

    # Collect all synsets that cannot be compressed
    necessary_synsets = get_necessary_synsets(y_words)

    # Map each sense to the lowest possible ancestor
    synset2necessary_hypernym = map_to_ancestor(input_word_senses, necessary_synsets)

    # Transform the synset mapping into a sense mapping
    old2new_wordsense = {get_sense_from_synset(word_sense): get_sense_from_synset(synset2necessary_hypernym[word_sense])
                         for word_sense in synset2necessary_hypernym}

    # Add the missing senses that were not added before because cannot be compressed
    for sense in input_word_senses:
        if sense not in old2new_wordsense:
            old2new_wordsense[sense] = sense

    # Save into a csv file
    data = {'original': list(old2new_wordsense.keys()),
            'compressed': list(old2new_wordsense.values())}

    original2compressed_sense_df = pd.DataFrame(data, columns=list(data.keys()))
    original2compressed_sense_df.to_csv("../resources/original2compressed_sense_mapping.csv", sep="\t")