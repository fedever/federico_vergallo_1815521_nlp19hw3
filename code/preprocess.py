# -*- coding: utf-8 -*-
from lxml import etree
import nltk
nltk.download('wordnet')
from nltk.corpus import wordnet as wn

filename = '../resources/semeval2007/semeval2007.data.xml'
context = etree.iterparse(filename, tag='sentence')

sensekey2synset_file = '../resources/semeval2007/semeval2007.gold.key.txt'
doc2sensekey = {x.split(' ')[0]: x.split(' ')[1:] for x in open(sensekey2synset_file).readlines()}

bn2wn_file = "../resources/babelnet2wordnet.tsv"
wn2bn = {x.split('\t')[1].replace("\n", "") : x.split('\t')[0] for x in open(bn2wn_file).readlines()}
bn2wn = {x.split('\t')[0] : x.split('\t')[1].replace("\n", "") for x in open(bn2wn_file).readlines()}

babelnet2lexnames_file = "../resources/babelnet2lexnames.tsv"
babelnet2lexnames = {x.split('\t')[0].replace("\n", ""): x.split('\t')[1].replace("\n", "") for x in open(babelnet2lexnames_file).readlines()}

babelnet2wndomains_file = "../resources/babelnet2wndomains.tsv"
babelnet2wndomains_pre = {x.split('\t')[0].replace("\n", ""): x.split('\t')[1:] for x in open(babelnet2wndomains_file).readlines()}

babelnet2wndomains = {}
for bn in babelnet2wndomains_pre:
    babelnet2wndomains[bn] = "_".join(babelnet2wndomains_pre[bn]).replace("\n", "")

for key in doc2sensekey:
    for sense in doc2sensekey[key]:
        if sense.endswith('\n'):
            new_sense = sense.replace("\n", "")
            doc2sensekey[key][doc2sensekey[key].index(sense)] = new_sense

def get_synset_from_sense_key(sense_key):
    return wn.lemma_from_key(sense_key).synset()

def get_sense_from_synset(synset):
    return "wn:" + str(synset.offset()).zfill( 8) + synset.pos()


generation_rate = 10000
i = 0
for event, sentence in context:
    text = []
    labels = []
    lex = []
    domains = []
    if  i%generation_rate == 0:
        print("Processate "+str(i)+" sentences")
    for tag in sentence:
        # Skipping punctuation
        if tag.attrib['pos'] == ".":
            continue
        
        #We are in wf tags
        if tag.tag == 'wf':
            labels.append(tag.attrib['lemma'])
            domains.append(tag.attrib['lemma'])
            lex.append(tag.attrib['lemma'])
        else: # We are in instance tag
            synset = get_synset_from_sense_key(doc2sensekey[tag.attrib['id']][0])
            sense = get_sense_from_synset(synset)
            labels.append(sense)
            lex.append(babelnet2lexnames[wn2bn[sense]])
            try:
                domains.append(babelnet2wndomains[wn2bn[sense]])
            except:
                domains.append("factotum")
        text.append(tag.attrib['lemma'])
        
        
    
    sentence.clear()
    with open("../resources/X_semeval2007_text.txt", "a", encoding='utf-8') as f:
        sent = ' '.join(text)
        f.write(sent+" \n")
    del text
    del sent
    
    
    with open("../resources/y_semeval2007_label.txt", "a", encoding='utf-8') as f:
        label_sent = ' '.join(labels)
        f.write(label_sent+" \n")
    del labels
    del label_sent
    
    with open("../resources/y_semeval2007_lex.txt", "a", encoding='utf-8') as f:
        lex_sent = ' '.join(lex)
        f.write(lex_sent+" \n")
    del lex
    del lex_sent
    
    with open("../resources/y_semeval2007_domains.txt", "a", encoding='utf-8') as f:
        domains_sent = ' '.join(domains)
        f.write(domains_sent+" \n")
    del domains
    del domains_sent
    
    i+=1

print("DONE")