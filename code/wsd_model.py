# -*- coding: utf-8 -*-
import tensorflow as tf
from tensorflow.keras.preprocessing.sequence import pad_sequences
from tensorflow.keras.models import Model
from tensorflow.keras.layers import LSTM, Embedding, Activation, concatenate, Concatenate, Input, Dense, Flatten, Lambda, TimeDistributed, multiply, Multiply, RepeatVector
from tensorflow.keras import initializers as initializers, regularizers, constraints
from tensorflow.keras.layers import InputSpec, Bidirectional
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
from tensorflow.keras.utils import to_categorical
from tensorflow.keras.optimizers import RMSprop, Adadelta, Adam
from tensorflow.keras import backend as K
from tensorflow.keras.utils import Sequence
from tensorflow.keras.utils import plot_model
import numpy as np
import pickle
import pandas as pd
from gensim.models import KeyedVectors
import statistics
import nltk
nltk.download('wordnet')
from nltk.corpus import wordnet as wn


# Useful wn transformations methods
def get_synset_from_sense(sense):
    return wn.synset_from_pos_and_offset(sense[-1], int(sense[-9:-1]))


def get_sense_from_synset(synset):
    return "wn:" + str(synset.offset()).zfill( 8) + synset.pos()


def get_input_output_files(path, flag_multitask):
    '''
    As title, returns input and output files.
    :param path:            string common path
    :param flag_multitask:  string True if multitasking setting
    :return: X_text, y_text, y_domains, y_lex input and output files
    '''
    with open(path+"X_semcor_text.txt", encoding="utf-8") as f:
        X_text = f.readlines()
    with open(path+"/y_semcor_label.txt", encoding="utf-8") as f:
        y_text = f.readlines()

    X_text = [text.strip("\n") for text in X_text]
    y_text = [text.strip("\n") for text in y_text]

    # For multitasking learning
    if flag_multitask:
        with open(path+"y_semcor_domains.txt", encoding="utf-8") as f:
            y_domains = f.readlines()
        with open(path+"/y_semcor_lex.txt", encoding="utf-8") as f:
            y_lex = f.readlines()

        y_domains = [domain.strip("\n") for domain in y_domains]
        y_lex = [lex.strip("\n") for lex in y_lex]

        return X_text, y_text, y_domains, y_lex
    else:
        return X_text, y_text,


def get_domain_lex_list(path):
    '''
    Returns the set of domains and lex
    :param path: string path to files
    :return: wndomains, wnlex set of domains and lex
    '''
    # Get input domains
    babelnet2wndomains_file = path+"babelnet2wndomains.tsv"
    babelnet2wndomains_pre = {x.split('\t')[0].replace("\n", ""): x.split('\t')[1:] for x in open(babelnet2wndomains_file).readlines()}
    babelnet2wndomains = {}
    for bn in babelnet2wndomains_pre:
        babelnet2wndomains[bn] = "_".join(babelnet2wndomains_pre[bn]).replace("\n", "")
    wndomains = list(set(babelnet2wndomains.values()))

    # Get input lex
    babelnet2lexnames_file = path+"babelnet2lexnames.tsv"
    babelnet2lexnames = {x.split('\t')[0].replace("\n", ""): x.split('\t')[1].replace("\n", "") for x in open(babelnet2lexnames_file).readlines()}
    wnlex = list(set(babelnet2lexnames.values()))

    return wndomains, wnlex


def get_compressing_vocabs(path):
    '''
    Returns the compressed vocabulary and its relative decompression vocab
    :param path: path to the file
    :return: original2compressed: dict map original wordnet sense to compressed one
    :return: compressed2original: dict map compressed wordnet sense to original ones
    '''
    # Build compression vocab
    original2compressed_sense_df = pd.read_csv(path + "original2compressed_semcor_sense_mapping.csv", delimiter="\t")
    original2compressed = {}
    for index, row in original2compressed_sense_df.iterrows():
        original2compressed[row['original']] = row['compressed']

    # Build decompression vocab
    compressed2original = {}
    for k, v in original2compressed.items():
        if v not in compressed2original:
            compressed2original[v] = []
        compressed2original[v].append(k)

    return original2compressed, compressed2original


def compress_output(y_text, original2compressed):
    '''
    Compress output text using compressed vocabulary
    :param y_text: list of list output labels text
    :param original2compressed: dict maps compression wn labels
    :return: y   : list of list compressed output
    '''
    y = []
    for sentence in y_text:
        new_sentence = []
        for sense in sentence.strip("\n").split(" "):
            if sense == "": continue
            if "wn:" in sense:
                new_sentence.append(original2compressed[sense])
            else:
                new_sentence.append(sense)
        y.append(" ".join(new_sentence))
    return y


def load_embeddings(make_embeddings, path):
    '''
    Load the pretrained embeddings files and returns the embedding matrix and the input vocabulary
    :param make_embeddings: bool True if have to build the embedding matrix, False if we need only to load
    :param path: string path to files
    :return: embedding_matrix, token_to_id, id_to_token
    '''
    if make_embeddings:
        # ---- Load embeddings ----
        wv_from_bin = KeyedVectors.load_word2vec_format(path+"embeddings2_en.vec", binary=True)
        EMBEDDING_DIM = wv_from_bin.vector_size
        vectors_number = wv_from_bin.vectors.shape[0]+2
        # ---- Create a weight matrix for words in training docs ----
        token_to_id = {el[1]: el[0]+2 for el in enumerate(wv_from_bin.vocab)}
        embedding_matrix = np.zeros((vectors_number, EMBEDDING_DIM))
        embedding_matrix[0], embedding_matrix[1]  = np.zeros(EMBEDDING_DIM), np.zeros(EMBEDDING_DIM) # PAD and UNK vectors
        for word, i in token_to_id.items():
            embedding_matrix[i] = wv_from_bin[word]
        # ---- Dump embedding matrix ----
        with open(path+"embedding_matrix.pkl", "wb") as f:
            pickle.dump(embedding_matrix, f, protocol=4)
        # ---- Dump token2id vocab ----
        token_to_id['<PAD>'] = 0
        token_to_id['<UNK>'] = 1
        with open(path+"token_to_id.pkl", "wb") as f:
            pickle.dump(token_to_id, f)
        # ---- Dump id2token vocab ----
        id_to_token = {v: k for k, v in token_to_id.items()}
        with open(path+"id_to_token.pkl", "wb") as f:
            pickle.dump(id_to_token, f)
    else:
        # ---- Load embeddings ----
        with open(path+"embedding_matrix.pkl", "rb") as f:
            embedding_matrix = pickle.load(f)
        # ---- Load token2id vocab ----
        with open(path+"token_to_id.pkl", "rb") as f:
            token_to_id = pickle.load(f)
        # ---- Dump id2token vocab ----
        with open(path+"id_to_token.pkl", "rb") as f:
            id_to_token = pickle.load(f)

    return embedding_matrix, token_to_id, id_to_token


def load_output_vocab(make_labels_dict, path, token_to_id):
    '''
    Load the output vocabulary. A label will be in the output vocab only 
    if it's a word sense or it's in the input vocab.
    :param make_labels_dict: bool True if we have to build it, False for just loading it
    :param path: string path to files
    :param token: dict input vocab
    :return: label_to_id, id_to_label
    '''
    if make_labels_dict :
        # ---- Encoding labels ----
        input_labels = []
        for line in y:
            line = line.split(" ")
            for word in line:
                input_labels.append(word.lower())

        labels = [label for label in list(set(input_labels))]
        # I'll save the label only if it's a word sense or it's in the input vocab
        labels = [label for label in labels if label in token_to_id or 'wn:' in label]

        # --- DICTIONARY ---
        label_to_id = dict()
        label_to_id["<PAD>"] = 0  # zero is not casual!
        label_to_id["<UNK>"] = 1  # OOV are mapped as <UNK>
        label_to_id.update({k: v + len(label_to_id) for v, k in enumerate(labels)})

        id_to_label = {v: k for k, v in label_to_id.items()}
        with open(path+"label_to_id.pkl", "wb") as f:
            pickle.dump(label_to_id, f)
        with open(path+"id_to_label.pkl", "wb") as f:
            pickle.dump(id_to_label, f)
    else:
        with open(path+"label_to_id.pkl", "rb") as f:
            label_to_id = pickle.load(f)
        with open(path+"id_to_label.pkl", "rb") as f:
            id_to_label = pickle.load(f)

    return label_to_id, id_to_label


def load_domain_vocab(make_domains_dict, path, token_to_id, wndomains):
    '''
    Load or build the domain vocab. A domain will be in the output vocab only if
    it's a domain or it's in the input vocab
    :param make_domains_dict: bool True if we have to build it, False for just loading it
    :param path: string path to files
    :param token_to_id: dict input vocab
    :param wndomains: list of wn domains
    :return: domain_to_id, id_to_domain output domain vocabulary
    '''
    if make_domains_dict :
        input_domains = list(set([word for sentence in y_domains for word in sentence.split(" ") if word]))
        domains = [word for word in input_domains if word in token_to_id or word in wndomains]
        domain_to_id = dict()
        domain_to_id['<PAD>'] = 0
        domain_to_id['factotum'] = 1
        del domains[domains.index('factotum')]
        domain_to_id.update({k: v + len(domain_to_id) for v, k in enumerate(domains)})
        id_to_domain = {v: k for k, v in domain_to_id.items()}
        with open(path+"domain_to_id.pkl", "wb") as f:
            pickle.dump(domain_to_id, f)
        with open(path+"id_to_domain.pkl", "wb") as f:
            pickle.dump(id_to_domain, f)
    else:
        with open(path+"domain_to_id.pkl", "rb") as f:
            domain_to_id = pickle.load(f)
        with open(path+"id_to_domain.pkl", "rb") as f:
            id_to_domain = pickle.load(f)
    return domain_to_id, id_to_domain


def load_lex_vocab(make_lex_dict, path, token_to_id, wnlex):
    '''
    Load or build the lex vocab. A lex will be in the output vocab only if
    it's a lex or it's in the input vocab
    :param make_lex_dict: bool True if we have to build it, False for just loading it
    :param path: string path to files
    :param token_to_id: dict input vocab
    :param wnlex: list of wn lex
    :return: lex_to_id, id_to_lex output domain vocabulary
    '''
    if make_lex_dict:
        input_lex = list(set([word for sentence in y_lex for word in sentence.split(" ") if word]))
        lex = [word for word in input_lex if word in token_to_id or word in wnlex]
        lex_to_id = dict()
        lex_to_id['<PAD>'] = 0
        lex_to_id['<UNK>'] = 1
        lex_to_id.update({k: v + len(lex_to_id) for v, k in enumerate(lex)})
        id_to_lex = {v: k for k, v in lex_to_id.items()}
        with open(path+"lex_to_id.pkl", "wb") as f:
            pickle.dump(lex_to_id, f)
        with open(path+"id_to_lex.pkl", "wb") as f:
            pickle.dump(id_to_lex, f)
    else: 
        with open(path+"lex_to_id.pkl", "rb") as f:
            lex_to_id = pickle.load(f)
        with open(path+"id_to_lex.pkl", "rb") as f:
            id_to_lex = pickle.load(f)

    return lex_to_id, id_to_lex


def transform_into_vectors(input_text, target_dict, flag_domain, MAX_LENGHT):
    '''
    Transforming input text in numeric form by means of vocabulary
    :param input_text: list of list of sentence
    :param target_dict: dict mapping between tokens and numbers
    :param flag_domain: bool if True use factotum as unk tag
    :param MAX_LENGHT : int padding size
    :return: padded numpy vectors
    '''
    arr = []
    if flag_domain: unk = 'UNK'
    else: unk = 'factotum'

    for sentence in input_text:
        line = []
        sentence = sentence.split(" ")
        for token in sentence:
            if token in target_dict:
                line.append(target_dict[token])
            else:
                line.append(target_dict[unk])

        arr.append(np.asarray(line))

    arr = np.asarray(arr)
    return pad_sequences(arr, truncating='pre', padding='post', maxlen=MAX_LENGHT, value=0)


def shuffle_split_sequence(input_seq, indices, VALIDATION_SPLIT):
    '''
    Shuffle a sequence using indices provided and split in test and train set
    :param input_seq: np array sequence
    :param indices: list of seq indices in random position
    :param VALIDATION_SPLIT: int portion of data to assign test set
    :return: input_seq_train, input_seq_val train and test set
    '''
    input_seq = input_seq[indices]
    nb_validation_samples = int(VALIDATION_SPLIT * input_seq.shape[0])
    input_seq_train = input_seq[:-nb_validation_samples]
    input_seq_val = input_seq[-nb_validation_samples:]
    return input_seq_train, input_seq_val


class mygenerator(Sequence):
    '''
    Keras generator for training
    '''
    def __init__(self, x_set, y_set, y_domains_set, y_lex_set, flag_multitask, batch_size):
        self.x, self.y = x_set, y_set
        if flag_multitask:
            self.y_domains, self.y_lex = y_domains_set, y_lex_set
        self.batch_size = batch_size

    def __len__(self):
        return int(np.ceil(len(self.x) / float(self.batch_size)))

    def __getitem__(self, idx):
        batch_x = self.x[idx * self.batch_size:(idx + 1) * self.batch_size]
        batch_y = self.y[idx * self.batch_size:(idx + 1) * self.batch_size]
        x = [sentence for sentence in batch_x] 
        y = [sentence for sentence in batch_y]
        
        if flag_multitask:
            batch_y_domains = self.y_domains[idx * self.batch_size:(idx + 1) * self.batch_size]
            batch_y_lex = self.y_lex[idx * self.batch_size:(idx + 1) * self.batch_size]
            y_domains = [sentence for sentence in batch_y_domains]
            y_lex = [sentence for sentence in batch_y_lex]
            return np.array(x), [to_categorical(y, num_classes=num_senses),
                                 to_categorical(y_domains, num_classes=num_domains),
                                 to_categorical(y_lex, num_classes=num_lex)]
        else:
            return np.array(x), to_categorical(y, num_classes=num_senses)


class AttentionWeightedAverage(Layer):
    '''
    Following Raganato's et. al paper, It computes the attention vector
    as the weighted sum of the hidden state vectors.
    :param inputs:
    :param lstm_units:
    :param MAX_LENGHT:
    :return:
    '''

    def __init__(self, return_attention=False, **kwargs):
        self.init = initializers.get('uniform')
        self.supports_masking = True
        self.return_attention = return_attention
        super(AttentionWeightedAverage, self).__init__(**kwargs)

    def build(self, input_shape):
        self.input_spec = [InputSpec(ndim=3)]
        assert len(input_shape) == 3

        self.w = self.add_weight(shape=(input_shape[2].value, 1),
                                 name='{}_w'.format(self.name),
                                 initializer=self.init,
                                 trainable=True)
        # self.trainable_weights = self.w
        super(AttentionWeightedAverage, self).build(input_shape)

    def call(self, h, mask=None):
        h_shape = K.shape(h)
        d_w, T = h_shape[0], h_shape[1]

        logits = K.dot(h, self.w)  # w^T h
        logits = K.reshape(logits, (d_w, T))
        alpha = K.exp(logits - K.max(logits, axis=-1, keepdims=True))  # exp

        # masked timesteps have zero weight
        if mask is not None:
            mask = K.cast(mask, K.floatx())
            alpha = alpha * mask
        alpha = alpha / K.sum(alpha, axis=1, keepdims=True)  # softmax
        c = K.sum(h * K.expand_dims(alpha), axis=1)  # c = h*alpha^T
        c_star = K.tanh(c)  # c^* = tanh(c)
        if self.return_attention:
            return [c_star, alpha]
        return c_star

    def get_output_shape_for(self, input_shape):
        return self.compute_output_shape(input_shape)

    def compute_output_shape(self, input_shape):
        output_len = input_shape[2]
        if self.return_attention:
            return [(input_shape[0], output_len), (input_shape[0], input_shape[1])]
        return (input_shape[0], output_len)

    def compute_mask(self, input, input_mask=None):
        if isinstance(input_mask, list):
            return [None] * len(input_mask)
        else:
            return None


### ---- Fun part: Model! ----
def build_wsd_model(vectors_number, EMBEDDING_DIM, embedding_matrix, lstm_units, MAX_LENGHT, num_senses, num_domains, num_lex, attention_flag, flag_multitask):
    '''
    Build the model with settings given by the flags
    :param vectors_number: int number of vectors in the embedding matrix
    :param EMBEDDING_DIM: int number of dimension of a vector
    :param embedding_matrix: dict enumerated mapped vectors
    :param lstm_units: int number of lstms cells
    :param MAX_LENGHT: int padding size
    :param num_senses: int number of output neurons for wsd task
    :param num_domains: int number of output neurons for domains task
    :param num_lex: int number of output neurons for lex task
    :param attention_flag: Bool True if want to use attention layer
    :param flag_multitask: Bool True if want to use multitasking setting
    :return: model
    '''

    # Imposing embedding layer to use pretrained embeddings
    embedding_layer = Embedding(vectors_number,
                                EMBEDDING_DIM,
                                weights=[embedding_matrix],
                                mask_zero=True,
                                trainable=False)

    sequence_input = Input(shape=(MAX_LENGHT,))
    embedded_sequences = embedding_layer(sequence_input)

    attention_flag = True
    if attention_flag:
        bilstm = Bidirectional(LSTM(units=lstm_units, return_sequences=True, recurrent_dropout=0.3))(embedded_sequences)
        c, alpha = AttentionWeightedAverage(return_attention=True)(bilstm)
        repeated_c = RepeatVector(MAX_LENGHT)(c)
        repeated_out = Lambda(lambda x: Concatenate()([x, repeated_c]))(bilstm)
        preds_wsd = TimeDistributed(Dense(num_senses, activation='softmax'), name='wsd')(repeated_out)
        if flag_multitask:
            preds_domains = TimeDistributed(Dense(num_domains, activation='softmax'), name='domains')(repeated_out)
            preds_lex = TimeDistributed(Dense(num_lex, activation='softmax'), name='lex')(repeated_out)
    else:
        bilstm = Bidirectional(LSTM(units=lstm_units, return_sequences=True, recurrent_dropout=0.3))(embedded_sequences)
        preds_wsd = TimeDistributed(Dense(num_senses, activation='softmax'), name='wsd')(bilstm)
        if flag_multitask:
            preds_domains = TimeDistributed(Dense(num_domains, activation='softmax'), name='domains')(bilstm)
            preds_lex = TimeDistributed(Dense(num_lex, activation='softmax'), name='lex')(bilstm)

    opt = Adam(lr=0.001)
    if flag_multitask:
        loss_list = ['categorical_crossentropy', 'categorical_crossentropy', 'categorical_crossentropy']
        wsd_model = Model(inputs=sequence_input,
                          outputs=[preds_wsd, preds_domains, preds_lex])
    else:
        loss_list = ['categorical_crossentropy']
        wsd_model = Model(inputs=sequence_input,
                          outputs=[preds_wsd])

    wsd_model.compile(loss=loss_list,
                      optimizer=opt,
                      metrics=['accuracy']
                      )

    wsd_model.summary()

    return wsd_model


def visualize_model(wsd_model):
    plot_model(wsd_model,
        to_file='model.png',
        show_shapes=True,
        show_layer_names=True,
        rankdir='TB'
    )


# Visualize training history
import matplotlib.pyplot as plt


def plot_history(history):
    loss_list = [s for s in history.history.history.keys() if 'loss' in s and 'val' not in s]
    val_loss_list = [s for s in history.history.history.keys() if 'loss' in s and 'val' in s]
    acc_list = [s for s in history.history.history.keys() if 'acc' in s and 'val' not in s]
    val_acc_list = [s for s in history.history.history.keys() if 'acc' in s and 'val' in s]

    if len(loss_list) == 0:
        print('Loss is missing in history')
        return

    # As loss always exists
    epochs = range(1, len(history.history.history[loss_list[0]]) + 1)

    # Loss
    plt.figure(1)
    colours_pairs = [('g', 'b'), ('r', 'c'), ('m', 'y'), ('k', 'w')]
    for l1, l2, color_pair in zip(loss_list, val_loss_list, colours_pairs):
        plt.plot(epochs, history.history.history[l1], color_pair[0], label='Training ' + l1 + '(' + str(str(format(history.history.history[l1][-1], '.5f')) + ')'))
        plt.plot(epochs, history.history.history[l2], color_pair[1], label='Validation ' + l2 + '(' + str(str(format(history.history.history[l2][-1], '.5f')) + ')'))

    plt.title('Loss')
    plt.xlabel('Epochs')
    plt.ylabel('Loss')
    plt.legend()

    # Accuracy
    plt.figure(2)
    for l1, l2, color_pair in zip(acc_list, val_acc_list, colours_pairs):
        plt.plot(epochs, history.history.history[l1], color_pair[0], label='Training ' + l1 + '(' + str(str(format(history.history.history[l1][-1], '.5f')) + ')'))
        plt.plot(epochs, history.history.history[l2], color_pair[1], label='Validation ' + l2 + '(' + str(str(format(history.history.history[l2][-1], '.5f')) + ')'))

    plt.title('Accuracy')
    plt.xlabel('Epochs')
    plt.ylabel('Accuracy')
    plt.legend()
    plt.show()

if __name__ == '__main__':
    # Flags
    flag_multitask = True
    compression_flag = True
    attention_flag = True
    # Constants
    VALIDATION_SPLIT = 0.2
    batch_size = 64
    lstm_units = 128
    path = "../resources/"

    # Open input and output files
    if flag_multitask:
        X_text, y, y_text_domains, y_text_lex = get_input_output_files(path, flag_multitask)
        wndomains, wnlex = get_domain_lex_list(path)
    else:
        X_text, y = get_input_output_files(path, flag_multitask)

    # Compressing
    if compression_flag:
        y = compress_output(path, y)

    # Load embeddings matrix and input vocab
    make_embeddings = False
    embedding_matrix, token_to_id, id_to_token = load_embeddings(make_embeddings, path)
    EMBEDDING_DIM = embedding_matrix.shape[1]
    vectors_number = embedding_matrix.shape[0]

    # Load output vocab
    make_labels_dict = False
    label_to_id, id_to_label = load_output_vocab(make_labels_dict, path, token_to_id)

    # Load multitasking output vocab
    if flag_multitask:
        make_domains_dict = False
        make_lex_dict = False
        domain_to_id, id_to_domain = load_domain_vocab(make_domains_dict, path, token_to_id, wndomains)
        lex_to_id, id_to_lex = load_lex_vocab(make_lex_dict, path, token_to_id, wnlex)

    # Statistics to compute padding size
    lengths = [len(i.split(" ")) for i in X_text]
    average = statistics.mean(lengths)
    MAX_LENGHT = int(round(average + 2 * np.std(lengths))) # padding size

    # Transform text in numeric form
    X = transform_into_vectors(X_text, token_to_id, False, MAX_LENGHT)
    y = transform_into_vectors(y, label_to_id, False, MAX_LENGHT)
    if flag_multitask:
        y_domains = transform_into_vectors(y_text_domains, domain_to_id, True, MAX_LENGHT)
        y_lex = transform_into_vectors(y_text_lex, lex_to_id, False, MAX_LENGHT)

    # Shuffle and split dataset in train and test sets
    indices = np.arange(X.shape[0])
    np.random.shuffle(indices)
    X_train, X_val = shuffle_split_sequence(X,indices, VALIDATION_SPLIT)
    y_train, y_val = shuffle_split_sequence(y,indices, VALIDATION_SPLIT)
    if flag_multitask:
        y_domains_train, y_domains_val = shuffle_split_sequence(y_domains,indices, VALIDATION_SPLIT)
        y_lex_train, y_lex_val = shuffle_split_sequence(y_lex,indices, VALIDATION_SPLIT)

    # Set the output model size
    num_senses = len(id_to_label.keys())
    if flag_multitask:
        num_domains = len(id_to_domain.keys())
        num_lex = len(id_to_lex.keys())
    else:
        num_domains, num_lex = 0, 0

    # Set the generators
    if flag_multitask:
        training_generator = mygenerator(X_train, y_train, y_domains_train, y_lex_train, True, batch_size)
        validation_generator = mygenerator(X_val, y_val, y_domains_val, y_lex_val, True, batch_size)
    else:
        training_generator = mygenerator(X_train, y_train, False, False, False, batch_size)
        validation_generator = mygenerator(X_val, y_val, False, False, False, batch_size)

    # Build the model
    wsd_model = build_wsd_model(vectors_number, EMBEDDING_DIM, embedding_matrix, lstm_units, MAX_LENGHT, num_senses, num_domains, num_lex, attention_flag, flag_multitask)

    # Use early stopping callback to stop before overfitting
    early_stopping = EarlyStopping(monitor='val_loss', patience=10)

    # Save weights when loss improves
    bst_model_path = path + "model_weights.hdf5"
    model_checkpoint = ModelCheckpoint(bst_model_path, save_best_only=True, save_weights_only=True, verbose=1)

    # happy learning!
    # Train model on dataset
    try:
        # wsd_model.load_weights(path+"compression_blstm_att.hdf5")
        model_history = wsd_model.fit_generator(generator=training_generator,
                                                validation_data=validation_generator,
                                                workers=5,
                                                callbacks=[model_checkpoint, early_stopping],
                                                epochs=100)
    except KeyboardInterrupt:
        print("Saving weights...")
        wsd_model.save_weights('model_weights_stop.hdf5')

    # Visualize loss and acc
    plot_history(wsd_model)

